use std::error::Error;
use std::time::Duration;
use chrono::{DateTime, Timelike, TimeZone, NaiveDateTime, Utc, Local};
use async_std::prelude::*;
use async_std::future;

use sqlx::{PgPool, Executor, Row};
use sqlx::postgres::PgArguments;

mod model;
mod webserver;
mod serde_naivedatetime;

async fn record_one_vehicle(
    pool: &mut PgPool,
    vehicle: model::BusSighting,
    now: &NaiveDateTime,
    beginning_of_timetable_day: &NaiveDateTime) -> Result<(), Box<dyn Error + Send + Sync>>
{
    // Check if a previous unfinished trip exists, and if so, finish it and mark it questionable
    let unfinished_previous_trips = sqlx::query_as!(
        model::BusTrip,
        "SELECT * FROM bus_trips
        WHERE vehicleref=$1
        AND tripref!=$2
        AND observedendtime IS NULL",
        vehicle.vehicleref,
        vehicle.tripref)
        .fetch_all(pool)
        .await?;
    
    for row in unfinished_previous_trips {
        eprintln!("Warning: {} had a previous unfinished trip before this one!", vehicle.vehicleref);
        sqlx::query!(
            "UPDATE bus_trips SET observedendtime=$1, questionablefinishtime=TRUE
            WHERE id=$2", *now, row.id)
            .execute(pool)
            .await?;
    }
    
    // Check whether the current unfinished trip exists in bus_trips, and create it if not
    let current_trip = sqlx::query_as!(
        model::BusTrip,
        "SELECT * FROM bus_trips
        WHERE vehicleref=$1
        AND tripref=$2
        AND observedstarttime BETWEEN $3 AND $4",
        vehicle.vehicleref,
        vehicle.tripref,
        *beginning_of_timetable_day,
        *now)
        .fetch_optional(pool)
        .await?;
    
    if let None = current_trip {
        sqlx::query!(
            "INSERT INTO bus_trips (
                vehicleref,
                lineref,
                directionref,
                blockref,
                tripref,
                originaimeddeparturetime,
                destinationaimedarrivaltime,
                observedstarttime,
                questionablefinishtime
            ) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, FALSE)",
            vehicle.vehicleref,
            vehicle.lineref,
            vehicle.directionref,
            vehicle.blockref,
            vehicle.tripref,
            vehicle.originaimeddeparturetime,
            vehicle.destinationaimedarrivaltime,
            *now)
            .execute(pool).await?;
    } else if let Some(trip) = current_trip {
        if trip.observedendtime.is_some() {
            sqlx::query!(
                "UPDATE bus_trips
                SET observedendtime=NULL
                WHERE id=$1",
                trip.id
            ).execute(pool).await?;
        }
    }

    sqlx::query!("INSERT INTO bus_sightings (
        time,
        vehicleref,
        lineref,
        longitude,
        latitude,
        delaysecs,
        percentage_,
        vehicleatstop,
        next_stoppointref,
        next_aimedarrivaltime,
        next_expectedarrivaltime,
        next_aimeddeparturetime
    ) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12)",
        *now,
        vehicle.vehicleref,
        vehicle.lineref,
        vehicle.longitude,
        vehicle.latitude,
        vehicle.delaysecs,
        vehicle.percentage,
        vehicle.vehicleatstop,
        vehicle.next_stoppointref,
        vehicle.next_aimedarrivaltime,
        vehicle.next_expectedarrivaltime,
        vehicle.next_aimeddeparturetime,
    ).execute(pool).await?;

    Ok(())
}

async fn check_and_record_vehicles(pool: &mut PgPool) -> Result<(), Box<dyn Error + Send + Sync>> {
    let mut inactive_vehicles: Vec<String> = Vec::new();
    let active_vehicles: Vec<model::BusSighting> = surf::get("https://data.foli.fi/siri/vm").await?
        .body_json::<serde_json::Value>().await?
        .get("result").ok_or("No result!")?
        .get("vehicles").ok_or("No vehicles!")?
        .as_object().ok_or("vehicles is not an object!")?
        .iter()
        .filter_map(|(id, vehicle)| {
            let val = serde_json::from_value(vehicle.clone()).ok();
            if let None = val { inactive_vehicles.push(id.to_string()); }
            val
        })
        .collect();
    let inactive_vehicles = inactive_vehicles;

    let now = Utc::now().naive_utc();

    // What's this nonsense? It's just the timetable day that lasts from 03:00 to the end of 02:59
    let beginning_of_timetable_day = {
        let now = Local::now();
        if now.hour() < 3 {
            now.date().pred().and_hms_opt(3,0,0)
                .unwrap_or(now.date().pred().and_hms_opt(4,0,0).ok_or("Invalid datetime!")?).naive_utc()
        } else {
            now.date().and_hms_opt(3,0,0)
                .unwrap_or(now.date().and_hms_opt(4,0,0).ok_or("Invalid datetime!")?).naive_utc()
        }
    };

    // Finish the trip record for any buses that just stopped being active
    for vehicle in inactive_vehicles {
        sqlx::query!(
            "UPDATE bus_trips
            SET observedendtime=$1
            WHERE vehicleref=$2
            AND observedendtime IS NULL",
            now,
            vehicle
        ).execute(pool).await?;
    }

    for vehicle in active_vehicles {
        let id = vehicle.vehicleref.clone();
        if let Err(e) = record_one_vehicle(pool, vehicle, &now, &beginning_of_timetable_day).await {
            eprintln!("Failed to record bus {}: {}", id, e);
        }
    }

    Ok(())
}

#[async_std::main]
async fn main() -> Result<(), Box<dyn Error>> {
    let mut pool = PgPool::new("postgresql://postgres@localhost/bussiloggeri").await?;
    
    async_std::task::spawn(webserver::run(pool.clone()));

    loop {
        future::ready(1u8).delay(Duration::from_millis(2500)).await;
        if let Err(e) = check_and_record_vehicles(&mut pool).await {
            eprintln!("Error! {}", e);
        }
    }
}