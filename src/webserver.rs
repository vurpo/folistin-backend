#![feature(generic_associated_types)]

use serde::{Deserialize, Serialize};
use chrono::NaiveDateTime;
use tide::{Request, Response};

use sqlx::{PgPool, Executor, Row};
use sqlx::postgres::PgArguments;

use std::pin::Pin;
use std::time::Instant;
use std::error::Error;
use std::default::Default;

use crate::model;

#[derive(Debug, Serialize)]
struct ErrorContent {
    pub error: String
}
impl ErrorContent {
    fn from_error<E: Error>(e: E) -> ErrorContent {
        ErrorContent { error: e.description().to_owned() }
    }
    fn from_str(e: &str) -> ErrorContent {
        ErrorContent { error: e.to_owned() }
    }
}

async fn index(_req: Request<PgPool>) -> Response {
    Response::new(200).body_string("{\"status\":\"OK\"}".to_owned())
}

async fn running_times(req: Request<PgPool>) -> Response {
    let mut pool = req.state();

    let times: Result<Vec<model::RunningTime>,_> = sqlx::query_as!(
        model::RunningTime,
        "SELECT * FROM running_times"
    ).fetch_all(&mut pool).await;

    match times {
        Ok(result) => {
            Response::new(200).body_json(&result).unwrap()
        },
        Err(e) => {
            Response::new(500).body_json(&ErrorContent::from_error(e)).unwrap()
        }
    }
}

async fn trips(req: Request<PgPool>) -> Response {
    let mut pool = req.state();

    let start: Result<NaiveDateTime, &'static str> = req.param::<i64>("start")
        .map_err(|_| "Parameter start must be an integer (UNIX timestamp)!")
        .and_then(|p| NaiveDateTime::from_timestamp_opt(p, 0).ok_or("Invalid timestamp start!"));
    let end: Result<NaiveDateTime, &'static str> = req.param::<i64>("end")
        .map_err(|_| "Parameter end must be an integer (UNIX timestamp)!")
        .and_then(|p| NaiveDateTime::from_timestamp_opt(p, 0).ok_or("Invalid timestamp end!"));
    let (start, end) = match (start, end) {
        (Err(e),_) => return Response::new(400).body_json(&ErrorContent::from_str(e)).unwrap(),
        (_,Err(e)) => return Response::new(400).body_json(&ErrorContent::from_str(e)).unwrap(),
        (Ok(start),Ok(err)) => (start,err)
    };

    let trips: Result<Vec<model::BusTrip>,_> = sqlx::query_as!(
        model::BusTrip,
        "SELECT * FROM bus_trips
        WHERE (observedendtime IS NULL AND tsrange($1, $2) @> observedstarttime)
        OR (observedendtime IS NOT NULL AND tsrange(observedstarttime, observedendtime) && tsrange($1, $2))",
        start, end
    ).fetch_all(&mut pool).await;

    match trips {
        Ok(result) => {
            Response::new(200).body_json(&result).unwrap()
        },
        Err(e) => {
            Response::new(500).body_json(&ErrorContent::from_error(e)).unwrap()
        }
    }
}

async fn trips_vehicleref(req: Request<PgPool>) -> Response {
    let mut pool = req.state();

    let start: Result<NaiveDateTime, &'static str> = req.param::<i64>("start")
        .map_err(|_| "Parameter start must be an integer (UNIX timestamp)!")
        .and_then(|p| NaiveDateTime::from_timestamp_opt(p, 0).ok_or("Invalid timestamp start!"));
    let end: Result<NaiveDateTime, &'static str> = req.param::<i64>("end")
        .map_err(|_| "Parameter end must be an integer (UNIX timestamp)!")
        .and_then(|p| NaiveDateTime::from_timestamp_opt(p, 0).ok_or("Invalid timestamp end!"));
    let (start, end) = match (start, end) {
        (Err(e),_) => return Response::new(400).body_json(&ErrorContent::from_str(e)).unwrap(),
        (_,Err(e)) => return Response::new(400).body_json(&ErrorContent::from_str(e)).unwrap(),
        (Ok(start),Ok(err)) => (start,err)
    };

    let vehicleref: String = req.param("vehicleref").unwrap();

    let trips: Result<Vec<model::BusTrip>,_> = sqlx::query_as!(
        model::BusTrip,
        "SELECT * FROM bus_trips
        WHERE (observedendtime IS NULL AND tsrange($1, $2) @> observedstarttime)
        OR (observedendtime IS NOT NULL AND tsrange(observedstarttime, observedendtime) && tsrange($1, $2))
        AND vehicleref=$3",
        start, end, vehicleref
    ).fetch_all(&mut pool).await;

    match trips {
        Ok(result) => {
            Response::new(200).body_json(&result).unwrap()
        },
        Err(e) => {
            Response::new(500).body_json(&ErrorContent::from_error(e)).unwrap()
        }
    }
}

async fn trips_lineref(req: Request<PgPool>) -> Response {

    let mut pool = req.state();

    let start: Result<NaiveDateTime, &'static str> = req.param::<i64>("start")
        .map_err(|_| "Parameter start must be an integer (UNIX timestamp)!")
        .and_then(|p| NaiveDateTime::from_timestamp_opt(p, 0).ok_or("Invalid timestamp start!"));
    let end: Result<NaiveDateTime, &'static str> = req.param::<i64>("end")
        .map_err(|_| "Parameter end must be an integer (UNIX timestamp)!")
        .and_then(|p| NaiveDateTime::from_timestamp_opt(p, 0).ok_or("Invalid timestamp end!"));
    let (start, end) = match (start, end) {
        (Err(e),_) => return Response::new(400).body_json(&ErrorContent::from_str(e)).unwrap(),
        (_,Err(e)) => return Response::new(400).body_json(&ErrorContent::from_str(e)).unwrap(),
        (Ok(start),Ok(err)) => (start,err)
    };

    let lineref: String = req.param("lineref").unwrap();

    let trips: Result<Vec<model::BusTrip>,_> = sqlx::query_as!(
        model::BusTrip,
        "SELECT * FROM bus_trips
        WHERE (observedendtime IS NULL AND tsrange($1, $2) @> observedstarttime)
        OR (observedendtime IS NOT NULL AND tsrange(observedstarttime, observedendtime) && tsrange($1, $2))
        AND lineref=$3",
        start, end, lineref
    ).fetch_all(&mut pool).await;

    match trips {
        Ok(result) => {
            Response::new(200).body_json(&result).unwrap()
        },
        Err(e) => {
            Response::new(500).body_json(&ErrorContent::from_error(e)).unwrap()
        }
    }
}

async fn trips_tripref(req: Request<PgPool>) -> Response {
    let mut pool = req.state();

    let start: Result<NaiveDateTime, &'static str> = req.param::<i64>("start")
        .map_err(|_| "Parameter start must be an integer (UNIX timestamp)!")
        .and_then(|p| NaiveDateTime::from_timestamp_opt(p, 0).ok_or("Invalid timestamp start!"));
    let end: Result<NaiveDateTime, &'static str> = req.param::<i64>("end")
        .map_err(|_| "Parameter end must be an integer (UNIX timestamp)!")
        .and_then(|p| NaiveDateTime::from_timestamp_opt(p, 0).ok_or("Invalid timestamp end!"));
    let (start, end) = match (start, end) {
        (Err(e),_) => return Response::new(400).body_json(&ErrorContent::from_str(e)).unwrap(),
        (_,Err(e)) => return Response::new(400).body_json(&ErrorContent::from_str(e)).unwrap(),
        (Ok(start),Ok(err)) => (start,err)
    };

    let tripref: String = req.param("tripref").unwrap();

    let trips: Result<Vec<model::BusTrip>,_> = sqlx::query_as!(
        model::BusTrip,
        "SELECT * FROM bus_trips
        WHERE (observedendtime IS NULL AND tsrange($1, $2) @> observedstarttime)
        OR (observedendtime IS NOT NULL AND tsrange(observedstarttime, observedendtime) && tsrange($1, $2))
        AND tripref=$3",
        start, end, tripref
    ).fetch_all(&mut pool).await;

    match trips {
        Ok(result) => {
            Response::new(200).body_json(&result).unwrap()
        },
        Err(e) => {
            Response::new(500).body_json(&ErrorContent::from_error(e)).unwrap()
        }
    }
}

async fn trips_tripid(req: Request<PgPool>) -> Response {
    let mut pool = req.state();

    let tripid: i64 = match req.param("tripid") {
        Ok(tripid) => tripid,
        Err(_) => return Response::new(400).body_json(&ErrorContent::from_str("tripid must be integer!")).unwrap()
    };

    let trip: Result<Option<model::BusTrip>>,_> = sqlx::query_as!(
        model::BusTrip,
        "SELECT * FROM bus_trips
        WHERE id=$1",
        tripid
    ).fetch_(&mut pool).await;

    match times {
        Ok(Some(result)) => {
            Response::new(200).body_json(&result).unwrap()
        },
        Ok(None) => {
            Response::new(404).body_json(&ErrorContent::from_str("No such trip")).unwrap()
        }
        Err(e) => {
            Response::new(500).body_json(&ErrorContent::from_error(e)).unwrap()
        }
    }
}

async fn data_date_range(req: Request<PgPool>) -> Response {
    let mut pool = req.state();

    let times: Result<Vec<model::RunningTime>,_> = sqlx::query_as!(
        model::RunningTime,
        "SELECT * FROM running_times"
    ).fetch_all(&mut pool).await;

    match times {
        Ok(result) => {
            Response::new(200).body_json(&result).unwrap()
        },
        Err(e) => {
            Response::new(500).body_json(&ErrorContent::from_error(e)).unwrap()
        }
    }
}

async fn data_lineref(req: Request<PgPool>) -> Response {
    let mut pool = req.state();

    let times: Result<Vec<model::RunningTime>,_> = sqlx::query_as!(
        model::RunningTime,
        "SELECT * FROM running_times"
    ).fetch_all(&mut pool).await;

    match times {
        Ok(result) => {
            Response::new(200).body_json(&result).unwrap()
        },
        Err(e) => {
            Response::new(500).body_json(&ErrorContent::from_error(e)).unwrap()
        }
    }
}

async fn data_vehicleref(req: Request<PgPool>) -> Response {
    let mut pool = req.state();

    let times: Result<Vec<model::RunningTime>,_> = sqlx::query_as!(
        model::RunningTime,
        "SELECT * FROM running_times"
    ).fetch_all(&mut pool).await;

    match times {
        Ok(result) => {
            Response::new(200).body_json(&result).unwrap()
        },
        Err(e) => {
            Response::new(500).body_json(&ErrorContent::from_error(e)).unwrap()
        }
    }
}

async fn data_tripid(req: Request<PgPool>) -> Response {
    let mut pool = req.state();

    let times: Result<Vec<model::RunningTime>,_> = sqlx::query_as!(
        model::RunningTime,
        "SELECT * FROM running_times"
    ).fetch_all(&mut pool).await;

    match times {
        Ok(result) => {
            Response::new(200).body_json(&result).unwrap()
        },
        Err(e) => {
            Response::new(500).body_json(&ErrorContent::from_error(e)).unwrap()
        }
    }
}

pub async fn run(pool: sqlx::PgPool) -> Result<(), Box<dyn Error + Send + Sync>> {
    let mut app = tide::with_state(pool);
    app.at("/api/").get(index);

    app.at("/api/running_times").get(running_times);

    app.at("/api/trips/:start/:end").get(trips);
    app.at("/api/trips/:start/:end/vehicle/:vehicleref").get(trips_vehicleref);
    app.at("/api/trips/:start/:end/line/:lineref").get(trips_lineref);
    app.at("/api/trips/:start/:end/trip/:tripref").get(trips_tripref);
    app.at("/api/trips/id/:id").get(trips_tripid);

    app.at("/api/data/:start/:end").get(data_date_range);
    app.at("/api/data/:start/:end/line/:lineref").get(data_lineref);
    app.at("/api/data/:start/:end/vehicle/:vehicleref").get(data_vehicleref);
    app.at("/api/data/trip/:tripid").get(data_tripid);

    app.listen("127.0.0.1:8080").await?;

    Ok(())
}