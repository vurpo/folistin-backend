use serde::{Serialize, Deserialize, Serializer, Deserializer};
use chrono::NaiveDateTime;

use crate::serde_naivedatetime::{naivedatetime, option_naivedatetime};

#[derive(Debug, Serialize, Deserialize)]
pub struct BusSighting {
    #[serde(skip)]
    pub id: Option<i32>,
    pub vehicleref: String,
    pub lineref: String,
    pub blockref: String,
    #[serde(rename = "__tripref")]
    pub tripref: String,
    pub directionref: String,
    pub longitude: f32,
    pub latitude: f32,
    pub delaysecs: i32,
    pub percentage: f32,
    pub vehicleatstop: bool,
    pub next_stoppointref: String,
    #[serde(with = "naivedatetime")]
    pub originaimeddeparturetime: NaiveDateTime,
    #[serde(with = "naivedatetime")]
    pub destinationaimedarrivaltime: NaiveDateTime,
    #[serde(with = "naivedatetime")]
    pub next_aimedarrivaltime: NaiveDateTime,
    #[serde(with = "naivedatetime")]
    pub next_expectedarrivaltime: NaiveDateTime,
    #[serde(with = "naivedatetime")]
    pub next_aimeddeparturetime: NaiveDateTime
}

// id serial PRIMARY KEY,
// vehicleref text NOT NULL,
// lineref text NOT NULL,
// directionref text NOT NULL,
// blockref text NOT NULL,
// tripref text NOT NULL,
// originaimeddeparturetime timestamp NOT NULL,
// destinationaimedarrivaltime timestamp NOT NULL,
// observedstarttime timestamp NOT NULL,
// observedendtime timestamp
#[derive(Debug, Serialize, Deserialize)]
pub struct BusTrip {
    pub id: i32,
    pub vehicleref: String,
    pub lineref: String,
    pub directionref: String,
    pub blockref: String,
    pub tripref: String,
    #[serde(with = "naivedatetime")]
    pub originaimeddeparturetime: NaiveDateTime,
    #[serde(with = "naivedatetime")]
    pub destinationaimedarrivaltime: NaiveDateTime,
    #[serde(with = "naivedatetime")]
    pub observedstarttime: NaiveDateTime,
    #[serde(default, with = "option_naivedatetime")]
    pub observedendtime: Option<NaiveDateTime>,
    pub questionablefinishtime: bool
}

// id serial PRIMARY KEY,
// starttime timestamp NOT NULL,
// endtime timestamp
#[derive(Debug, Serialize, Deserialize)]
pub struct RunningTime {
    pub id: i32,
    #[serde(with = "naivedatetime")]
    pub starttime: NaiveDateTime,
    #[serde(default, with = "option_naivedatetime")]
    pub endtime: Option<NaiveDateTime>,
}