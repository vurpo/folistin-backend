use serde::{Serialize, Deserialize, Serializer, Deserializer};
use chrono::NaiveDateTime;

pub struct IntTimestampVisitor;

impl<'de> serde::de::Visitor<'de> for IntTimestampVisitor {
    type Value = NaiveDateTime;

    fn expecting(&self, formatter: &mut std::fmt::Formatter) -> std::fmt::Result {
        formatter.write_str("an integer representing a UNIX timestamp")
    }

    fn visit_u64<E>(self, v: u64) -> Result<Self::Value, E>
    where
        E: serde::de::Error,
    {
        self.visit_i64(v as i64)
    }

    fn visit_i64<E>(self, v: i64) -> Result<Self::Value, E>
    where
        E: serde::de::Error,
    {
        NaiveDateTime::from_timestamp_opt(v, 0).ok_or(E::custom("Invalid timestamp!"))
    }
}
pub struct OptionIntTimestampVisitor;

impl<'de> serde::de::Visitor<'de> for OptionIntTimestampVisitor {
    type Value = Option<NaiveDateTime>;

    fn expecting(&self, formatter: &mut std::fmt::Formatter) -> std::fmt::Result {
        formatter.write_str("an integer representing a UNIX timestamp")
    }

    fn visit_none<E>(self) -> Result<Self::Value, E>
    where
        E: serde::de::Error,
    {
        Ok(None)
    }

    fn visit_some<D>(self, de: D) -> Result<Self::Value, D::Error>
    where 
        D: Deserializer<'de>,
    {
        de.deserialize_i64(IntTimestampVisitor)
            .map(|t| Some(t))
    }
}

pub mod naivedatetime {
    use serde::{Serialize, Deserialize, Serializer, Deserializer};
    use chrono::NaiveDateTime;
    use crate::serde_naivedatetime::IntTimestampVisitor;
    pub fn deserialize<'de, D>(de: D) -> Result<NaiveDateTime, D::Error> where D: Deserializer<'de> { 
        de.deserialize_any(IntTimestampVisitor)
    }

    pub fn serialize<S>(datetime: &NaiveDateTime, ser: S) -> Result<S::Ok, S::Error> where S: Serializer {
        ser.serialize_i64(datetime.timestamp())
    }
}

pub mod option_naivedatetime {
    use serde::{Serialize, Deserialize, Serializer, Deserializer};
    use chrono::NaiveDateTime;
    use crate::serde_naivedatetime::{IntTimestampVisitor, OptionIntTimestampVisitor};
    pub fn deserialize<'de, D>(de: D) -> Result<Option<NaiveDateTime>, D::Error> where D: Deserializer<'de> {
        
        de.deserialize_any(OptionIntTimestampVisitor)
    }

    pub fn serialize<S>(datetime: &Option<NaiveDateTime>, ser: S) -> Result<S::Ok, S::Error> where S: Serializer {
        let datetime = datetime.map(|d| d.timestamp());
        if datetime.is_some() { ser.serialize_some(&datetime) }
        else { ser.serialize_none() }
    }
}