CREATE TABLE bus_sightings(
    id serial PRIMARY KEY,
    donotdelete boolean DEFAULT FALSE NOT NULL,
    time timestamp NOT NULL,
    vehicleref text NOT NULL,
    lineref text NOT NULL,
    longitude real NOT NULL,
    latitude real NOT NULL,
    delaysecs int NOT NULL,
    percentage_ real NOT NULL,
    vehicleatstop boolean NOT NULL,
    next_stoppointref text NOT NULL,
    next_aimedarrivaltime timestamp NOT NULL,
    next_expectedarrivaltime timestamp NOT NULL,
    next_aimeddeparturetime timestamp NOT NULL
);

CREATE TABLE bus_trips(
    id serial PRIMARY KEY,
    donotdelete boolean DEFAULT FALSE NOT NULL,
    vehicleref text NOT NULL,
    lineref text NOT NULL,
    directionref text NOT NULL,
    blockref text NOT NULL,
    tripref text NOT NULL,
    originaimeddeparturetime timestamp NOT NULL,
    destinationaimedarrivaltime timestamp NOT NULL,
    observedstarttime timestamp NOT NULL,
    observedendtime timestamp, -- should be NULL until the trip has been observed to finish running
    questionablefinishtime boolean NOT NULL -- indicates this trip was possibly unfinished
);

CREATE TABLE running_times(
    id serial PRIMARY KEY,
    starttime timestamp NOT NULL,
    endtime timestamp
);